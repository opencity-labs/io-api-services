# IO Api

Microservizio che implementa le stesse API Services dell'App.Io usate dagli Enti o dai fornitori per l'invio di messaggi sull'app.

L'obiettivo è ricevere i messaggi destinati a IO, inviarli effettivamente a IO ma averne anche una copia da inviare a un'altra piattaforma.

## Getting started

Configura il servizio destinatario mediante una variabile di ambiente e lancia il microservizio

Agli applicativi esistenti cambia l'hostname di IO e fallo puntare a questo microservizio.

Tutti i messaggi verranno inviati sia a IO che al destinatario configurato.

## Installation

Il microservizio è distribuito mediante immagine Docker e si può configurare mediante variabili d'ambiente

## Configuration

| Variable  | Default | Description
|-----------|---------|------------
| APP_IO_REAL_SERVER | https://api.io.pagopa.it/api/v1 | Indirizzo delle API di APP.IO per cui fare da proxy


## Support

In caso di difficoltà aprire una issue avendo sempre cura di indicare la versione che si sta usando e descrivere bene eventuali messaggi di errore.

## Roadmap

- [ ] wrapper puro delle chiamte dei messaggi

  - [ ] POST `/profiles`
  - [ ] POST `/messages`
  - [ ] GET `/messages/${fiscal_code}/${id}`
  
Vedi anche https://docs.pagopa.it/io-guida-tecnica/api/api-messaggi/
  
- [ ] aggiungere una chiamata API rest per ogni messaggio
- [ ] invio a un topic di Kafka di un evento per ogni messaggio ricevuto
- [ ] implementazione API dei servizi
  - [ ] GET `/services`
  - [ ] GET `/services/{service_id}`
  - [ ] PUT `/services/{service_id}`
  - [ ] PUT `/services/{service_id}/keys`
  - [ ] PUT `/services/{service_id}/logo`

Vedi anche https://docs.pagopa.it/io-guida-tecnica/api/api-servizi 

## Contributing

E' possibile proporre modifiche mediante una MR.

## Authors and acknowledgment

Opencity Labs S.r.l.


## License

AGPL 3.0

## Project status

Under active development